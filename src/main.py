
from flask import Flask
app = Flask(__name__)

@app.route('/api/ciao')
def saludoAPI():
  response = {"message": "Hola UTN!!!"}
  return response

@app.route('/page/ciao')
def saludoPages():
  return '<h1 style="padding-top: 50px; width:100%; text-align: center;">Hola UTN!!!</h1>'
	
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='7000',debug=1)
	